package main

import "fmt"

type Price struct {
	Value int
	Day int
}

// Minimal and maximal elements without checking if the min day is before the max day
func MaxMin (prices []Price) (Price, Price) {
	if len(prices) < 2 {
		return prices[0], prices[0]
	}

	mid := len(prices)/2

	max1, min1 := MaxMin(prices[mid:])
	max2, min2 := MaxMin(prices[:mid])

	var newMin Price
	var newMax Price
	if min2.Value < min1.Value {
		newMin = min2
	} else {
		newMin = min1
	}

	if max2.Value > max1.Value {
		newMax = max2
	} else {
		newMax = max1
	}

	return newMax, newMin
}

// At least the size of the array is constant
func MaxCoupleByDay (prices [4]Price) (Price, Price) {
	minPrice := prices[0]
	maxPrice := prices[0]
	maxDiff := maxPrice.Value - minPrice.Value

	for i := range prices {
		for j := range prices {
			if prices[i].Day > prices[j].Day {
				if prices[i].Value - prices[j].Value > maxDiff {
					minPrice = prices[j]
					maxPrice = prices[i]
					maxDiff = prices[i].Value - prices[j].Value
				}
			}
		}
	}

	return maxPrice, minPrice
}

// Maximal and minimal elements with checking the day
func MaxMinWithDay (prices []Price) (Price, Price) {
	if len(prices) < 2 {
		return prices[0], prices[0]
	}

	mid := len(prices)/2

	max1, min1 := MaxMin(prices[mid:])
	max2, min2 := MaxMin(prices[:mid])

	return MaxCoupleByDay([4]Price{min1, max1, min2, max2})
}

func main() {

	prices := []Price{
		{200, 1},
		{150, 2},
		{300, 3},
		{400, 4},
		{100, 5},
		{50, 6},
	}

	max, min := MaxMin(prices)
	fmt.Println("min: ", min)
	fmt.Println("max: ", max)

	max, min = MaxMinWithDay(prices)
	fmt.Println("min: ", min)
	fmt.Println("max: ", max)

	prices = []Price{
		{200, 1},
		{150, 2},
		{300, 3},
		{50, 4},
		{100, 5},
		{500, 6},
	}
	max, min = MaxMinWithDay(prices)
	fmt.Println("min: ", min)
	fmt.Println("max: ", max)
}