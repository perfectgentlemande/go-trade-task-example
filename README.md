# Trade task
For educational purposes.  

This task was from the 'Computer Science Distilled' book by Wladston Ferreira Filho. 
The task was to find the best day to purchase and the best one to sell the item/stock etc using the 'Divide-And-Conquer' method.
  
You may see from the code the function `func MaxMin (prices []Price) (Price, Price) {}` finds the maximal and minimal elements in the slice using the 'Divide-And-Conquer' method. 
This is not enough to solve the task as after the division of the slice into two subslices and finding the max/min elements you need to merge them guaranteeing the 2 things:
- the day with the maximal price to sell is the same day you purchase or goes after it;
- the difference between the days of selling and the purchasing is maximal.

So that is solved by the `func MaxMinWithDay (prices []Price) (Price, Price) {}` where the call of the `func MaxCoupleByDay (prices [4]Price) (Price, Price) {}` function merges the dates by the difference.
As you may see `func MaxCoupleByDay (prices [4]Price) (Price, Price) {}` works with brute force but this is bounded by the constant size of an array (4).