package main

import (
	"testing"
)

type TestCase struct {
	 Prices []Price
	 Expected [2]Price
}

func TestMaxMinWithDay(t *testing.T) {
	var tests = []TestCase{
		{
			Prices: []Price{
				{200, 1},
				{150, 2},
				{20, 3},
				{110, 4},
				{12, 5},
				{500, 6},
				{6, 7},
			},
			Expected: [2]Price {
				{500, 6},
				{20, 3},
			},
		},
		{
			Prices: []Price{
				{900, 1},
				{150, 2},
				{20, 3},
				{200, 4},
				{12, 5},
				{60, 6},
				{6, 7},
			},
			Expected: [2]Price {
				{200, 4},
				{20, 3},
			},
		},
		{
			Prices: []Price{
				{900, 1},
				{150, 2},
				{20, 3},
				{950, 4},
				{12, 5},
				{60, 6},
				{6, 7},
			},
			Expected: [2]Price {
				{950, 4},
				{20, 3},
			},
		},
	}

	for i := range tests {
		max, min := MaxMinWithDay(tests[i].Prices)

		if [2]Price{max, min} != tests[i].Expected {
			t.Errorf("got %v, want %v", [2]Price{max, min}, tests[i].Expected)
		}
	}
}

